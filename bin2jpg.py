#!/usr/bin/python3

__version__ = "0.2.2"
__prog__ = "bin2jpg"
__authors__ = ["Thomas Robinson"]
__copyright__ = "GNU GPLv3 (https://choosealicense.com/licenses/gpl-3.0/)"

import sys, argparse, os, math, numpy, PIL.Image
from hilbertcurve.hilbertcurve import HilbertCurve

fileToRead = ""
fileToWrite = ""

Parser = argparse.ArgumentParser(prog=__prog__, usage=f'{sys.argv[0]} [-v] [--skip <bytes>] [-bw | -gs | -c3] [-s] <input file> [output file]')
Parser.add_argument('-v', '--verbose', action='store_true', help='Print verbose information')
Parser.add_argument('--skip', nargs=1, type=int, action='store', help='Skips number of heading bytes (e.g., to skip a common header)')
Conversion = Parser.add_mutually_exclusive_group()
Conversion.add_argument('-bw', '--blackandwhite', action='store_true', help=f'Convert to 1bit per pixel image (default)')
Conversion.add_argument('-gs', '--grayscale', action='store_true', help=f'Convert to 8bit per pixel grayscale image')
Conversion.add_argument('-c3', '--colorized', action='store_true', help=f'Convert to 24bpp (8bit per primary) colorized image')
Parser.add_argument('-c', '--curve', action='store_true', help='Draw along hilbert curve instead of left to right')
Parser.add_argument('-s', '--show', action='store_true', help='Show image only, do not save (Overrides any output file)')
Parser.add_argument('files', nargs=argparse.REMAINDER, metavar='input and output files')
Options = Parser.parse_args(sys.argv[1:])

if Options.verbose: print("Verbose mode")

if len(Options.files) == 1:
    fileToRead = Options.files[0]
    if Options.show:
        if Options.verbose: print(f'Input file: {fileToRead}, displaying image output only, not saving')
    else:
        fileToWrite = f'{Options.files[0]}.jpg'
        if Options.verbose: print(f'Input file: {fileToRead}, Outputting to: {fileToWrite}')
elif len(Options.files) == 2:
    fileToRead = Options.files[0]
    if Options.show:
        if Options.verbose: print(f'Input file: {fileToRead}, displaying image output only, no reason for the output file specified')
    else:
        fileToWrite = Options.files[1]
        if Options.verbose: print(f'Input file: {fileToRead}, Outputting to: {fileToWrite}')
else:
    Parser.parse_args(['-h'])
    sys.exit(0)

if Options.grayscale:
    if Options.curve:
        root = math.floor(math.log(math.sqrt(os.path.getsize(fileToRead))) / math.log(2))
        if root == 0:
            print('File size too small, needs more data for input')
            sys.exit(0)
        Curve = HilbertCurve(root, 2)
        ImageSize = math.floor(math.sqrt(2**(2*root)))
        if Options.verbose: print(f'Grayscale Mode/Hilbert Curve. Image size: {ImageSize}x{ImageSize}')
    else:
        ImageSize = math.floor(math.sqrt(os.path.getsize(fileToRead)))
        if Options.verbose: print(f'Grayscale Mode. Image size: {ImageSize}x{ImageSize}')
elif Options.colorized:
    if Options.curve:
        root = math.floor(math.log(math.sqrt(os.path.getsize(fileToRead) / 3)))
        if root == 0:
            print('File size too small, needs more data for input')
            sys.exit(0)
        Curve = HilbertCurve(root, 2)
        ImageSize = math.floor(math.sqrt(2**(2*root)))
        if Options.verbose: print(f'Colorized Mode/Hilbert Curve. Image size: {ImageSize}x{ImageSize}')
    else:
        ImageSize = math.floor(math.sqrt(os.path.getsize(fileToRead) / 3))
        if Options.verbose: print(f'Colorized Mode. Image size: {ImageSize}x{ImageSize}')
else:
    if Options.curve:
        root = math.floor(math.log(math.sqrt(os.path.getsize(fileToRead) * 8)) / math.log(2))
        if root == 0:
            print('File size too small, needs more data for input')
            sys.exit(0)
        Curve = HilbertCurve(root, 2)
        ImageSize = math.floor(math.sqrt(2**(2 * root)))
        if Options.verbose: print(f'1BPP Mode/Hilbert Curve. Image size: {ImageSize}x{ImageSize}')
    else:
        ImageSize = math.floor(math.sqrt(os.path.getsize(fileToRead) * 8))
        if Options.verbose: print(f'1BPP Mode. Image size: {ImageSize}x{ImageSize}')

if ImageSize == 0: # This probably shouldn't ever happen, but lets not make any assumptions
    print('File size too small, needs more data for input')
    sys.exit(0)
Canvas = numpy.float32(PIL.Image.new("RGB", ((ImageSize), (ImageSize)), 0))

with open(fileToRead, "rb") as BinFile:
    if Options.skip:
        if Options.verbose: print(f'Skipping {Options.skip[0]} bytes')
        BinFile.read(Options.skip[0])
    if Options.colorized:
        Byte = BinFile.read(3)
    else:
        Byte = BinFile.read(1)
    CurrentLine = 0
    CurrentPixel = 0
    CurrentIndex = 0
    while Byte:
        if Options.grayscale:
            if Options.curve:
                CurrentPixel, CurrentLine = Curve.coordinates_from_distance(CurrentIndex)
                Canvas[CurrentLine, CurrentPixel][0:] = Byte[0]
                CurrentIndex += 1
                if CurrentIndex == 2**(2*root)-1:
                    if Options.verbose: print(f'Reached limit of image size ({CurrentIndex} pixels)')
                    break
            else:
                Canvas[CurrentLine, CurrentPixel][0:] = Byte[0]
                CurrentPixel += 1
                if CurrentPixel == ImageSize:
                    CurrentPixel = 0
                    CurrentLine += 1
                    if CurrentLine == ImageSize:
                        if Options.verbose: print(f'Reached limit of image size ({CurrentLine * ImageSize} pixels)')
                        break
        elif Options.colorized:
            if Options.curve:
                if len(Byte) == 3:
                    CurrentPixel, CurrentLine = Curve.coordinates_from_distance(CurrentIndex)
                    Canvas[CurrentLine, CurrentPixel] = [Byte[0], Byte[1], Byte[2]]
                    CurrentIndex += 1
                    if CurrentIndex == 2**(2*root)-1:
                        if Options.verbose: print(f'Reached limit of image size ({CurrentIndex} pixels)')
                        break
                else:
                    if Options.verbose: print(f'Ran out of bytes. Wrote {CurrentLine}x{CurrentPixel} image')
                    break
            else:
                if len(Byte) == 3:
                    Canvas[CurrentLine, CurrentPixel] = [Byte[0], Byte[1], Byte[2]]
                    CurrentPixel += 1
                    if CurrentPixel == ImageSize:
                        CurrentPixel = 0
                        CurrentLine += 1
                        if CurrentLine == ImageSize:
                            if Options.verbose: print(f'Reached limit of image size ({CurrentLine * ImageSize} pixels)')
                            break
                else:
                    if Options.verbose: print(f'Ran out of bytes. Wrote {CurrentLine}x{CurrentPixel} image')
                    break
        else:
            if Options.curve:
                for BinaryDigit in bin(Byte[0])[2:]:
                    CurrentPixel, CurrentLine = Curve.coordinates_from_distance(CurrentIndex)
                    if BinaryDigit == "1": Canvas[CurrentLine, CurrentPixel][0:] = 255
                    CurrentIndex += 1
                    if CurrentIndex == 2**(2*root)-1: # This will only break the for loop
                        if Options.verbose: print(f'Reached limit of image size: ({CurrentIndex} pixels)')
                        break
                if CurrentIndex == 2 ** (2 * root) - 1: # Needed to break because of for loop
                    if Options.verbose: print(f'Looped limit of image size: ({CurrentIndex} pixels)')
                    break
            else:
                for BinaryDigit in bin(Byte[0])[2:]:
                    if BinaryDigit == "1": Canvas[CurrentLine, CurrentPixel][0:] = 255
                    CurrentPixel += 1
                    if CurrentPixel == ImageSize:
                        CurrentPixel = 0
                        CurrentLine += 1
                        if CurrentLine == ImageSize:
                            if Options.verbose: print(f'Reached limit of image size: ({CurrentLine * ImageSize} pixels)')
                            break
                if CurrentLine == ImageSize: # Probably never will be necessary, but just in case
                    if Options.verbose: print(f'Looped limit of image size: ({CurrentLine * ImageSize} pixels)')
                    break
        if Options.colorized:
            Byte = BinFile.read(3)
        else:
            Byte = BinFile.read(1)
BinFile.close()

if Options.show:
    PIL.Image.fromarray(numpy.uint8(Canvas)).show()
else:
    PIL.Image.fromarray(numpy.uint8(Canvas)).save(fileToWrite)
    if Options.verbose: print(f'Wrote {os.path.getsize(fileToWrite)} bytes to {fileToWrite}')