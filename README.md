# bin2jpg
bin2jpg is a python script that takes an arbitrary file as binary input, and converts it into an image.

## Description
Born out of a meme circulating on Facebook that described the concept of being able to knit the DooM installer based on its binary code being represented by different kinds of stitches, this program aims to take an arbitrary file as binary input, and converts the resulting binary representation into an image suitable for upload to a service which can then produce a cross-stitch or other sewing pattern from that image.

## Usage
```
bin2jpg [-v] [--skip <bytes>] [-bw | -gs | -c3] [-c] [-s] <input file> [output file]
```
-v | --verbose: Activate verbose mode

--skip <bytes>: Skips bytes number of heading bytes (e.g., to skip a common header)

-bw | --blackandwhite: Activate 1 bit per pixel black and white mode

-gs | --grayscale: Activate grayscale (8bpp mode)

-c3 | --colorized: Activate 24bpp (8bit per primary) colorized mode

-c | --curve: Draw along a space filling (hilbert) curve instead of drawing left to right, top to bottom

-s | --show: Display output image only, do not save (Overrides any input image)

Input file is required. Output file is optional (software will append .jpg to the name of the input file as necessary)

## Notes
As of this writing, this software is entirely pre-alpha, proof of concept only. It is not intended to be highly useful, and should not be used seriously. This software has not been thoroughly tested, and has not been optimized for memory or processing speed. If used on a very large file, the resulting process may take a very long time to complete and/or may crash (including crashing your operating system).

## Requirements:
python3 (code is not compatible with python2 at this time, you will get floating point errors if you try)

argparse (should be included with your python3 install, but you may need to `pip3 install argparse` if not)

numpy

pillow (or other PIL compatible imaging library)

hilbertcurve (Library from https://github.com/galtay/hilbertcurve required to draw along space filling curve)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)